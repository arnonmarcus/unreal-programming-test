// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelGameMode.h"

#include "BreakoutGameInstance.h"
#include "PaddleController.h"
#include "LevelPlayerState.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"


// Called when the game starts or when spawned
void ALevelGameMode::BeginPlay()
{
	Super::BeginPlay();

	// Sanity check that all Widget classes have been provided for instantiation in the GameMode Blueprint:
	if (!StartMenuClass            ) UE_LOG(LogTemp, Warning, TEXT("Missing Start Menue Class!"))
	if (!LevelHUDClass             ) UE_LOG(LogTemp, Warning, TEXT("Missing Level HUD Class!"));;
	if (!LevelFailureScreenClass   ) UE_LOG(LogTemp, Warning, TEXT("Missing Level Fauilure Screen Class!"));
	if (!LevelCompletionScreenClass) UE_LOG(LogTemp, Warning, TEXT("Missing Level Completion Screen Class!"));
	if (!GameCompletionScreenClass ) UE_LOG(LogTemp, Warning, TEXT("Missing Game Completion Screen Class!"));
	
	// Capture pointers to relevant classes at level-staer:
	PlayerController = GetWorld()->GetFirstPlayerController<APaddleController>();	
	PlayerState = PlayerController->GetPlayerState<ALevelPlayerState>();
	BreakoutGameInstance = GetGameInstance<UBreakoutGameInstance>();
	
	if (BreakoutGameInstance->GameInstanceInitialized) {
		// Start manu has already passed, initialized the opened level for paying:

		// Show the player HUD:
		LevelCompleted = false;		
		LevelHUD = CreateWidget<UUserWidget>(GetWorld(), LevelHUDClass);
		LevelHUD->AddToViewport();

		// Disable mouse interaction:			
		FInputModeGameOnly InputMode;
		PlayerController->SetInputMode(InputMode);
		PlayerController->SetShowMouseCursor(false);
	} else {
		// Game hasn't started yet, initialize the game and show the start menu:
		
		// Initialized remaining lives only once for the whole game (as opposed to per-level)
		BreakoutGameInstance->GameInstanceInitialized = true;
		BreakoutGameInstance->ResetLives();

		// Show the start menu:
		StartMenu = CreateWidget<UUserWidget>(GetWorld(), StartMenuClass);
		StartMenu->AddToViewport();

		// Enable mouse interaction:
		FInputModeUIOnly InputMode;
		GetWorld()->GetFirstPlayerController()->SetInputMode(InputMode);
		GetWorld()->GetFirstPlayerController()->SetShowMouseCursor(true);	
	}
}

void ALevelGameMode::FailLevel()
{
	// Level play failed (ball fell through).
	
	// Reset the lives remaining in case the user wants to try again and not quit:
	BreakoutGameInstance->ResetLives();

	// Remove the player HUD and show the level failure screen:
	if (LevelHUD) LevelHUD->RemoveFromParent();
	LevelFailureScreen = CreateWidget<UUserWidget>(GetWorld(), LevelFailureScreenClass);
	LevelFailureScreen->AddToViewport();

	// Enable mouse interactions for the buttons
	FInputModeUIOnly InputMode;
	PlayerController->SetInputMode(InputMode);
	PlayerController->SetShowMouseCursor(true);	
}

void ALevelGameMode::LevelComplete()
{
	// All blocks are broken, level is completed successfully:

	// Prevent level-failing due to having the ball fall-through while the success-screen is shown
	LevelCompleted = true;

	// Remove the player HUD and show the level completion screen for 1 second before continuing to the next level:
	if (LevelHUD) LevelHUD->RemoveFromParent();
	LevelCompletionScreen = CreateWidget<UUserWidget>(GetWorld(), LevelCompletionScreenClass);
	LevelCompletionScreen->AddToViewport();
	GetWorldTimerManager().SetTimer(SwapLevelsTimer, this, &ALevelGameMode::LoadNextLevel, 1.0f, false);
}

void ALevelGameMode::LoadNextLevel()
{
	// Check next level index, and decide if to open it or finish the game:
	int32 NextLevel = BreakoutGameInstance->CurrentLevelIndex + 1;
	if (NextLevel >= Levels.Num()) {
		BreakoutGameInstance->GameIsWon = true;

		// Remove the player HUD
		if (LevelHUD) LevelHUD->RemoveFromParent();

		// Remove the level completion screen that was triggered by the last level completion
		if (LevelCompletionScreen) LevelCompletionScreen->RemoveFromParent();		

		// Show the game completion screen
		GameCompletionScreen = CreateWidget<UUserWidget>(GetWorld(), GameCompletionScreenClass);
		GameCompletionScreen->AddToViewport();

		// Enable mouse interaction for quitting the game
		FInputModeUIOnly InputMode;
		PlayerController->SetInputMode(InputMode);
		PlayerController->SetShowMouseCursor(true);	
	} else {
		// More levels yet to play, open the next one:
		BreakoutGameInstance->CurrentLevelIndex = NextLevel;
		OpenLevelOfCurrentIndex();;
	}
}

bool ALevelGameMode::KillPlayer()
{
	if (BreakoutGameInstance->LivesLeft)
		BreakoutGameInstance->LivesLeft--;
	if (BreakoutGameInstance->LivesLeft ||
		BreakoutGameInstance->GameIsWon || LevelCompleted)
			return false; // Bail on failing if the player still have lives remaining, or the level or game was won

	// Delay-trigger the level failing process
	GetWorldTimerManager().SetTimer(SwapLevelsTimer, this, &ALevelGameMode::FailLevel, 1.0f, false);
	
	return true;
}

void ALevelGameMode::BreakBrick()
{
	PlayerState->BrokenBricks++;
	if (PlayerState->BrokenBricks == PlayerState->TotalBricks) // All bricks are broken - delay-trigger level completion
		GetWorldTimerManager().SetTimer(SwapLevelsTimer, this, &ALevelGameMode::LevelComplete, 1.0f, false);
}

// Blueprint getters for the player HUD's progress bars
float ALevelGameMode::GetHealth() { return static_cast<float>(BreakoutGameInstance->LivesLeft) / static_cast<float>(BreakoutGameInstance->InitialLives); }
float ALevelGameMode::GetBricks() { return static_cast<float>(PlayerState->BrokenBricks) / static_cast<float>(PlayerState->TotalBricks); }

// Blueprint-wired from the level failing screen for re-loading the current level (also used for loading the next one)
void ALevelGameMode::OpenLevelOfCurrentIndex()
{
	UGameplayStatics::OpenLevel(this, FName(*Levels[BreakoutGameInstance->CurrentLevelIndex]), true);
}

// Blueprint-wired from the game completion screen for restarting the game
void ALevelGameMode::RestartGame()
{
	BreakoutGameInstance->GameIsWon = false;
	BreakoutGameInstance->CurrentLevelIndex = 0;
	BreakoutGameInstance->ResetLives();
	OpenLevelOfCurrentIndex();
}

