// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "LevelPlayerState.generated.h"

/**
 * This class holds the player's per-level state
 * Note: The state and logic of this class could technically exist in the GameMode class
 * It is segregated into here for semantic reasons (grouping by purpose, here relating to transient player-state).
 */
UCLASS()
class BREAKOUT_API ALevelPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	// State made public for accessibility by the GameMode class
	uint32 TotalBricks;
	uint32 BrokenBricks;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};