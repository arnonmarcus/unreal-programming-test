// Fill out your copyright notice in the Description page of Project Settings.


#include "Brick.h"
#include "Ball.h"
#include "LevelGameMode.h"

// Sets default values
ABrick::ABrick()
{
	// Note: Ticking disabled and override omitted for performance (unneeded for this class)
	PrimaryActorTick.bCanEverTick = false;
	
	Brick = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Brick"));
	RootComponent = Brick;
	
	// Setup frictionless rigid box physics
	Brick->SetEnableGravity(false);
	Brick->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Brick->SetCollisionProfileName(TEXT("PhysicsActor"));
}

// Called when the game starts or when spawned
void ABrick::BeginPlay()
{
	Super::BeginPlay();

	// Reset the bricks hits per-level-start and set up the hit event handler
	HitsTookSoFar = 0;
	LevelGameMode = Cast<ALevelGameMode>(GetWorld()->GetAuthGameMode());	
	Brick->OnComponentHit.AddDynamic(this, &ABrick::OnHit);
}

void ABrick::Damage()
{
	if (++HitsTookSoFar == TotalHitsToBreak) {
		// Took as much hits as are needed to break - destroy it:
		Destroy();
		LevelGameMode->BreakBrick(); // Notify the game-mode so it can update the score to the finish-line
		OnKill(); // Providing a Blueprint Event hook for potential extensibility (unused in this project) 
	} else {
		// Took hits but not enough to break it - set it's mesh to it's damaged material (if one is provided)
		if (HitMaterial) Brick->SetMaterial(0, HitMaterial);
		OnDamage(); // Providing a Blueprint Event hook for potential extensibility (unused in this project) 
	}
}

void ABrick::OnHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (Cast<ABall>(Other)) Damage(); // Brick was hit by the ball - damage it
}