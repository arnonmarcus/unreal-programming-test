// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// #include "Ball.h"

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PaddleController.generated.h"


/**
 * 
 */
UCLASS()
class BREAKOUT_API APaddleController : public APlayerController
{
	GENERATED_BODY()

public:
	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

protected:	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Relevant actor handles:
	class APaddlePawn *PaddlePawn;

	// Pass-through functions for user input handling, down o the possessed pawn:
	void SlidePaddle(float AxisValue);
	void LaunchBall();
};
