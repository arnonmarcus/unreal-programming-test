// Fill out your copyright notice in the Description page of Project Settings.


#include "PaddleController.h"

#include "PaddlePawn.h"
#include "Kismet/GameplayStatics.h"
#include "Camera/CameraActor.h"

// Called when the game starts or when spawned
void APaddleController::BeginPlay()
{
	Super::BeginPlay();

	PaddlePawn = Cast<APaddlePawn>(GetPawn());

	// Grab the first camera actor instance in the level (there will always be only one)
	// Keeping it within the level allows to view through the camera in the viewport, which can be useful
	// Would rather find a nicer way of doing it, but given it's only at begin-play, shouldn't be an issue
	TArray<AActor*> CameraActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACameraActor::StaticClass(), CameraActors);
	FViewTargetTransitionParams Params;
	SetViewTarget(CameraActors[0], Params);	
}

// User input handles pass-through functions, down to the possessed pawn:
void APaddleController::SlidePaddle(float AxisValue) { PaddlePawn->SlidePaddle(AxisValue); }
void APaddleController::LaunchBall() { PaddlePawn->LaunchBall(); }

// Called to bind functionality to input
void APaddleController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// Wire user-input handling through the through functions, down to the possessed pawn:
	EnableInput(this);
	InputComponent->BindAxis("Slide", this, &APaddleController::SlidePaddle);
	InputComponent->BindAction("LaunchBall", IE_Pressed, this, &APaddleController::LaunchBall);
}