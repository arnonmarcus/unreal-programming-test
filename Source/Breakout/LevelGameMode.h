// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LevelGameMode.generated.h"

/**
 * Game logic between/across levels and GUI handling (menu, screens and HUD)
 */
UCLASS()
class BREAKOUT_API ALevelGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	// Exposed interface for use by other classes (Paddle and Brick)
	bool KillPlayer();
	void BreakBrick();	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// The array of all the level names in-order (needs to be filled manually in the Blueprint)
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowedPrivateAccess = "True"))
	TArray<FString> Levels;
	
	// Blueprint getters for the HUD Widget flow-graph
	UFUNCTION(BlueprintPure)
	float GetHealth();
	UFUNCTION(BlueprintPure)
	float GetBricks();

	// Blueprint callables for reloading the current level and for restarting the game
	UFUNCTION(BlueprintCallable)
	void OpenLevelOfCurrentIndex();
	UFUNCTION(BlueprintCallable)
	void RestartGame();
	
	// Blueprint classes for UI widgets and their instances:	
	UPROPERTY(EditAnywhere, Category = "UMG")
	TSubclassOf<UUserWidget> LevelCompletionScreenClass;
	UPROPERTY(EditAnywhere, Category = "UMG")
	UUserWidget* LevelCompletionScreen;
	UPROPERTY(EditAnywhere, Category = "UMG")
	TSubclassOf<UUserWidget> LevelFailureScreenClass;
	UPROPERTY(EditAnywhere, Category = "UMG")
	UUserWidget* LevelFailureScreen;
	UPROPERTY(EditAnywhere, Category = "UMG")
	TSubclassOf<UUserWidget> GameCompletionScreenClass;
	UPROPERTY(EditAnywhere, Category = "UMG")
	UUserWidget* GameCompletionScreen;
	UPROPERTY(EditAnywhere, Category = "UMG")
	TSubclassOf<UUserWidget> StartMenuClass;
	UPROPERTY(EditAnywhere, Category = "UMG")
	UUserWidget* StartMenu;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UMG", meta = (BlueprintProtected = "True"))
	TSubclassOf<class UUserWidget> LevelHUDClass;
	UPROPERTY(EditAnywhere, Category = "UMG")
	UUserWidget* LevelHUD;
	
	// Callbacks use with a timer
	void FailLevel();
	void LevelComplete();	
	void LoadNextLevel();
	
	// Relevant handles initialized per level-startup
	class UBreakoutGameInstance *BreakoutGameInstance;
	class APaddleController *PlayerController;
	class ALevelPlayerState *PlayerState;

	// Control variable to prevent level-failing due to having the ball fall-through while the success-screen is shown
	bool LevelCompleted;

	// Timer for all the delayed-triggering of screen transitions
	FTimerHandle SwapLevelsTimer;
};