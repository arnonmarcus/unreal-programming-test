// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Brick.generated.h"

UCLASS()
class BREAKOUT_API ABrick : public AActor
{
	GENERATED_BODY()
	
public:	
	// Note: Ticking disabled and override omitted for performance (unneeded for this class)
	ABrick();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Number of hits this brick can take before being destroyed (overridable at the Blueprint on a per-instance level)
	UPROPERTY(EditAnywhere, Category = Setup)
	int32 TotalHitsToBreak = 1;
	
	// Components for Mesh and damaged-material (for when a brick gets damaged but not destroyed):
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Setup)
	UStaticMeshComponent *Brick;
	UPROPERTY(EditAnywhere, Category = Setup)
	UMaterialInstance *HitMaterial;
	
	// Brick hit event handler:
	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent *MyComp, AActor *Other, class UPrimitiveComponent *OtherComp, FVector NormalImpulse, const FHitResult & Hit);
	
	// Providing Blueprint Event hooks for potential extensibility (unused in this project)
	UFUNCTION(BlueprintImplementableEvent)
	void OnKill();	
	UFUNCTION(BlueprintImplementableEvent)
	void OnDamage();
	
	// Sharable damaging logic for hit-handlers of both regular bricks (this class) and sliding ones (sub-class)
	void Damage();

	// Relevant actor handles:
	class ALevelGameMode* LevelGameMode;
	
	// Hit counter while the brick is not destroyed
	int32 HitsTookSoFar;
};
