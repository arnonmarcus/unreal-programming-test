// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PaddlePawn.generated.h"

UCLASS()
class BREAKOUT_API APaddlePawn : public APawn
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this pawn's properties
	APaddlePawn();
	
	// User input handlers:
	void SlidePaddle(float AxisValue);
	void LaunchBall();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Area of effect regions:
	UPROPERTY(EditAnywhere, Category = Setup)
	class UBoxComponent *LaunchEnabledRegion;
	UPROPERTY(EditAnywhere, Category = Setup)
	class UBoxComponent *DeathRegion;
	
	// Area of effect regions enter/exit callbacks:
	UFUNCTION()
	void OnBeginOverlapDeath(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// Providing Blueprint Event hooks for potential extensibility (unused in this project) 
	UFUNCTION(BlueprintImplementableEvent)
	void OnSlide(); 
	UFUNCTION(BlueprintImplementableEvent)
	void OnLaunchBall(); // Providing a Blueprint Event hook for potential extensibility (unused in this project) 
	
	// Paddle mesh and movement components:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Setup)
	UStaticMeshComponent *Paddle;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UFloatingPawnMovement *Movement;

	// Blueprint-Overridable impulse strength for ball launching 
	UPROPERTY(EditAnywhere, Category = Setup)
	float LaunchSpeed;

	// Relevant actor handles:
	class ABall *Ball;
	class ALevelGameMode* LevelGameMode;

	// Control variable for when the ball can vs. can-not be launched
	bool bCanLaunch;
};
