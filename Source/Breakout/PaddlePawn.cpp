// Fill out your copyright notice in the Description page of Project Settings.

#include "PaddlePawn.h"

#include "Ball.h"
#include "LevelGameMode.h"

#include "Components/BoxComponent.h"
#include "GameFramework/FloatingPawnMovement.h"

// Sets default values
APaddlePawn::APaddlePawn()
{
	PrimaryActorTick.bCanEverTick = true;

	Paddle = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Paddle"));
    RootComponent = Paddle;
	
    Paddle->SetEnableGravity(false);

	// Paddle->SetConstraintMode(EDOFMode::XZPlane); <- Fixed: More specific constraints are set at the Blueprint level
    // (Rotation disabled and Translation constrained to X-axis only, so the Ball can never push the paddle down)
	
    Paddle->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    Paddle->SetCollisionProfileName(TEXT("PhysicsActor"));

	// Area of effect for where Launching the ball is enabled, should not cause collisions, only emmit an overlap event
	// Extents of the Box Collision region are set at the Blueprint's Viewport for ease of control/visualization
	LaunchEnabledRegion = CreateDefaultSubobject<UBoxComponent>(TEXT("LaunchEnabledRegion"));	
	LaunchEnabledRegion->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	
	// Bottom area of effect for player death, should not cause collisions, only emmit an overlap event
	// Extents of the Box Collision region are set at the Blueprint's Viewport for ease of control/visualization
	DeathRegion = CreateDefaultSubobject<UBoxComponent>(TEXT("DeathRegion"));
	DeathRegion->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	
    Movement = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("Movement"));

	LaunchSpeed = 250.0f;
	Ball = nullptr; // Will be set with the ball actor while it is within the LaunchEnabledRegion
	bCanLaunch = false; // Will be set to true while the ball is within the LaunchEnabledRegion
}

// Called when the game starts or when spawned
void APaddlePawn::BeginPlay()
{
	Super::BeginPlay();

	// Grab a pointer to the custom game-mode once at span/init-time for later use: 
	LevelGameMode = Cast<ALevelGameMode>(GetWorld()->GetAuthGameMode());

	// Setup entrance/exit callback for area of effect regions:
	DeathRegion->OnComponentBeginOverlap.AddDynamic(this, &APaddlePawn::OnBeginOverlapDeath);
	LaunchEnabledRegion->OnComponentBeginOverlap.AddDynamic(this, &APaddlePawn::OnBeginOverlap);
	LaunchEnabledRegion->OnComponentEndOverlap.AddDynamic(this, &APaddlePawn::OnEndOverlap);
}

void APaddlePawn::SlidePaddle(float AxisValue)
{
	// Move the paddle left or right based on user input: 
	AddMovementInput(FVector(AxisValue, 0.0f, 0.0f), 1.0f, false);

	// Update the area of effect for the launch region so it moved with the paddle
	// (Workaround, due to it not following the static mesh component, even though it is parented under it - not sure why)
	LaunchEnabledRegion->SetRelativeLocation(Paddle->GetRelativeLocation());

	OnSlide(); // Providing a Blueprint Event hook for potential extensibility (unused in this project) 
}

void APaddlePawn::LaunchBall()
{
	if (bCanLaunch) {
		bCanLaunch = false; // Ensure the user can not double/multi-launch the ball by tapping on the input repeatedly
		Ball->Launch(LaunchSpeed);
		OnLaunchBall();	// Providing a Blueprint Event hook for potential extensibility (unused in this project) 
	}
}

void APaddlePawn::OnBeginOverlapDeath(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// If the ball fell to it's death, notify the game-mode to decrement Lives, and if the's any left, reset the ball
	Ball = Cast<ABall>(OtherActor);
	if (Ball && !LevelGameMode->KillPlayer()) Ball->Reset();
}

void APaddlePawn::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Ball = Cast<ABall>(OtherActor);
	if (Ball) bCanLaunch = true; // The ball entered the Launching area of effect so it can be launched while here
}

void APaddlePawn::OnEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Ball = Cast<ABall>(OtherActor);
	if (Ball) bCanLaunch = false; // The ball exited the Launching area of effect so it can no longer be launched now
}