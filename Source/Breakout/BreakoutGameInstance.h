// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "BreakoutGameInstance.generated.h"

/**
 * The main overridable C++/Blueprint class who's instance is a singleton that persists across levels, used for:
 * - Tracking the current level index
 * - Tracking remaining player-lives across levels
 * - Ensuring that certain things happen only once per game-session as opposed to per-level  
 */
UCLASS()
class BREAKOUT_API UBreakoutGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	// Blueprint-overridable initial lives count
	UPROPERTY(EditAnywhere, Category = Setup)
    uint32 InitialLives;
	
	uint32 LivesLeft; // current-lives tracking (carries across levels)
	
    void ResetLives(); // Should happen only once at startup, and then only on re-play of failed levels
	
    int32 CurrentLevelIndex = 0;

	// Single-occurence control variables:
    bool GameInstanceInitialized = false;
    bool GameIsWon = false;
	
};
