// Fill out your copyright notice in the Description page of Project Settings.

#include "Ball.h"

#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"

// Sets default values
ABall::ABall()
{
	// Note: Ticking disabled and override omitted for performance (unneeded for this class)
	PrimaryActorTick.bCanEverTick = false;

	// Defaulted values for the blueprint-overridable initialization properties
	InitialVelocity = FVector(200.0f, 0.0f, -150.0f);
	StartingLocation = FVector(000.0f, 0.0f, 200.0f);
	
	BallSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	RootComponent = BallSphere;

	// Setup frictionless bouncy ball physics
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	ProjectileMovement->ProjectileGravityScale = 0.0f;
	ProjectileMovement->bShouldBounce = true;
	ProjectileMovement->Bounciness = 1.0f;
	ProjectileMovement->Friction = 0.0f;
}

void ABall::Launch(float LaunchSpeed)
{
	// If the ball is coming it hot, and the launch was invoked on the way in,
	// then accumulating the velocities may end-up counteracting them and stopping the ball (undesired).
	// Conversely if it's coming in hot and leaving hot and the launch was invoked on the way out,
	// then accumulating the velocities may end-up with a doubling effect and get overblown (also undesired).
	// So, only accumulate when it makes sense, otherwise just set the launch speed by itself:
	if (LaunchSpeed > ProjectileMovement->Velocity.Z)
		ProjectileMovement->Velocity.Z = LaunchSpeed;
	else
		ProjectileMovement->Velocity.Z += LaunchSpeed;
}

void ABall::Reset()
{
	// Reset the location before the timer to signify to the player that it's being reset
	BallSphere->SetWorldLocation(StartingLocation);
	ProjectileMovement->Velocity = FVector(0.0f, 0.0f, 0.0f);
	FTimerHandle UnusedHandle;
	GetWorldTimerManager().SetTimer(UnusedHandle, this, &ABall::DoReset, 2, false);
}

void ABall::DoReset()
{
	// Handle the resetting of the ball to it's initial state (pending a timer)
	// TODO: Could add some randomness here to make it less predictable

	// Reset the location even when it's already been reset before the timer, for code-reused in BeginPlay()
	BallSphere->SetWorldLocation(StartingLocation);
	ProjectileMovement->Velocity.X = InitialVelocity.X;
	ProjectileMovement->Velocity.Z = InitialVelocity.Z;
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	
	Reset();
}

