// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingBrick.h"
#include "Ball.h"

#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
AMovingBrick::AMovingBrick()
{
	// Note: Ticking disabled and override omitted for performance (unneeded for this class)
	PrimaryActorTick.bCanEverTick = false;

	// Set-up sliding movement component (Further fleshed out in the Blueprint)
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	ProjectileMovement->ProjectileGravityScale = 0.0f;
	ProjectileMovement->bShouldBounce = true;
	ProjectileMovement->Bounciness = 1.0f;
	ProjectileMovement->Velocity.X = 180;
	ProjectileMovement->Friction = 0.0f;
}

void AMovingBrick::OnHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// If the brick is hit by the ball, it gets damage (and if needed, destroyed)
	// Otherwise, the brick is sliding sideways and hit some wall or other brick, so it should bounce back:
	if (Cast<ABall>(Other)) Damage(); else  ProjectileMovement->Velocity.X = -ProjectileMovement->Velocity.X;
}