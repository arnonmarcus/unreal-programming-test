// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelPlayerState.h"

#include "Brick.h"
#include "Kismet/GameplayStatics.h"


// Called when the game starts or when spawned
void ALevelPlayerState::BeginPlay()
{
	Super::BeginPlay();

	// Count the brinks in the current level at start-up
	TArray<AActor*> CameraActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABrick::StaticClass(), CameraActors);
	TotalBricks = CameraActors.Num();
	
	BrokenBricks = 0; // Reset the broken bricks counter on each level/play
}
