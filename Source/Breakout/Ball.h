// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ball.generated.h"

UCLASS()
class BREAKOUT_API ABall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Note: Ticking disabled and override omitted for performance (unneeded for this class)
	ABall();

	void Launch(float LaunchSpeed);
	void Reset();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Blueprint-Overridable starting location and initial velocity
	UPROPERTY(EditAnywhere, Category = Setup)
	FVector InitialVelocity;
	UPROPERTY(EditAnywhere, Category = Setup)
	FVector StartingLocation;

	// Collision+Mesh and movement components
	UPROPERTY(EditAnywhere, Category = Setup)
	class USphereComponent *BallSphere;
	UPROPERTY(EditAnywhere)
	class UProjectileMovementComponent *ProjectileMovement;
	
	void DoReset();
};
