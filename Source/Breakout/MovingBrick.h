// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Brick.h"
#include "MovingBrick.generated.h"

/**
 * Sub-class of the Brick class, adding sliding movement and direction-flipping on collision with walls/bricks
 */
UCLASS()
class BREAKOUT_API AMovingBrick : public ABrick
{
	GENERATED_BODY()
		
public:
	// Note: Ticking disabled and override omitted for performance (unneeded for this class)
	AMovingBrick();

	// Simple physics-enable movement controller for sliding bricks
	UPROPERTY(EditAnywhere)
	class UProjectileMovementComponent *ProjectileMovement;
	
protected:
	// Brick hit event handler:
	virtual void OnHit(UPrimitiveComponent *MyComp, AActor *Other, class UPrimitiveComponent *OtherComp, FVector NormalImpulse, const FHitResult & Hit) override;
};
