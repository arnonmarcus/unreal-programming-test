# CerebralFix Senior Unreal Engine Coding Test #

Details:
-
Provided are the source file for the project. It uses Unreal Engine 4.27.2<br>

There are 2 levels (Level01 and Level02)<br>
<i>(The main menue feature is implemented without the need for a dummy empty level)</i><br>

All requirements we met with an addition of:
-
* HUD with scrollbars for life-point and remaining blocks to destory<br>
* Multiple levels<br>
* Win screens<br>

Controls (keyoard):
-
* Move Paddle Left: "A" key of "Left Arrow" key<br>
* Move Paddle Right: "D" key of "Right Arrow" key<br>
* Boost Ball: "S" key or "Spacebar" key<br>
<br>
Boosting is only possible around a small rectangular area around the Paddle.